# Balance Robot CAD files
Free CAD projects for all 3D printed parts.

The full_robot directory shows how the parts should be assembled.

# Assembly information
To fixate the "distance pipes" on the plates, I drilled the M3 screws directly into the pipes.

Be wary of the ordering for the screws. The middle distance pipes have to be attached before the motor holder because the motor holder covers the holes for the pipes.

# Parts (bought most stuff on Amazon)
- Usongshine Nema 17 Stepper Motor 1.5A High Torque 420mN.m
- AZDelivery GY-521 MPU-6050
- HRB 2600mAh 11.1V 35C 3S LiPo Akku Pack
- AZDelivery DRV8825 stepper driver
- AZDelivery Breadboard + cables
- Some M3 nuts and screws (10mm, 14mm) M3 nuts
    - the screw heads should be flat
- XT60 battery connector
- power on switch
- Black Pill (https://stm32-base.org/boards/STM32F411CEU6-WeAct-Black-Pill-V2.0.html)
- flashed via SWD with St-Link